"use strict";

var app = angular.module('node_starter_client');

app.controller("HomeCtrl", ["$scope",
    function($scope) {
        $scope.welcome = 'Welcome';
        $scope.title = 'Home';
    }
]);