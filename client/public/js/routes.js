"use strict";
var app = angular.module('node_starter_client');


app.config(['$routeProvider', '$locationProvider', '$httpProvider',
    function($routeProvider, $locationProvider, $httpProvider) {
        $routeProvider.
        when('/', {
            templateUrl: 'partials/home',
            controller: 'HomeCtrl'
        }).
        otherwise({
            redirectTo: '/'
        });

        $locationProvider.html5Mode(true);
    }]
);