#!/usr/bin/env node

/**
 * Module dependencies.
 */

var app = require('../app');
var http = require('http');
var https = require('https');
var fs = require('fs');

var socketRoutes = require('../routes/socket');
var config = require('../config/config').cfg;
var routes = require('../routes/index');

var applogger = require('../applogger');
var log = applogger.logger.child({
    component: 'www'
});

/**
 * Get port from environment and store in Express.
 */

var port = normalizePort(process.env.PORT || '3000');
var httpsPort = process.env.HTTPS_PORT || 443;
app.set('port', port);


/**
 * Create HTTP server.
 */

//-- load certificates
var options = {
  key: fs.readFileSync(process.env.KEY_LOC, 'utf8'),
  cert: fs.readFileSync(process.env.CERT_LOC, 'utf8')
};


//-- create https server
var serverHttps = https.createServer(options, app);

//-- create http server and redirect to https server
//   note: config.hostname is https://....
var server = http.createServer(function(req, res) {
    var path =  process.env.APP_HOSTNAME + req.url;
    res.writeHead(302, {'Location': path});
    res.end();
});

//-- All other requests
app.get('*', routes.routeToIndex);

/**
 * Listen on provided port, on all network interfaces.
 */
server.listen(port);
serverHttps.listen(httpsPort);


/**
* Socket IO stuff
*/
var socketio = require('socket.io')(serverHttps);
socketRoutes.handleConnect(socketio);
app.socketio = socketio;

serverHttps.on('error', onError);
serverHttps.on('listening',
  function() {
    log.info('listening on ' + port.toString());
  }
);

/**
 * Normalize a port into a number, string, or false.
 */

function normalizePort(val) {
  var port = parseInt(val, 10);

  if (isNaN(port)) {
    // named pipe
    return val;
  }

  if (port >= 0) {
    // port number
    return port;
  }

  return false;
}

/**
 * Event listener for HTTP server "error" event.
 */

function onError(error) {
  if (error.syscall !== 'listen') {
    throw error;
  }

  var bind = typeof port === 'string'
    ? 'Pipe ' + port
    : 'Port ' + port

  // handle specific listen errors with friendly messages
  switch (error.code) {
    case 'EACCES':
      console.error(bind + ' requires elevated privileges');
      process.exit(1);
      break;
    case 'EADDRINUSE':
      console.error(bind + ' is already in use');
      process.exit(1);
      break;
    default:
      throw error;
  }
}
