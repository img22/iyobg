#!/bin/bash
# load nvm, adjust node version
NVM_PATH=`command -v nvm`
if [ -z $NVM_PATH ]; then
    [ -s "$NVM_DIR/nvm.sh" ] && . "$NVM_DIR/nvm.sh";  # This loads nvm
fi

nvm use 6.9.1

if [ -z $APP_HOSTNAME ]; then
    echo "Need APP_HOSTNAME env variable"
    exit 1
fi

if [ -z $APP_CLIENT ]; then
    echo "Need APP_CLIENT env variable"
    exit 1
fi

if [ -z $APP_NAME ]; then
    echo "Need APP_NAME env variable"
    exit 1
fi

if [ -z $APP_ROOT ]; then
    APP_ROOT="$HOME/$APP_NAME"
    echo "using default app root $APP_ROOT"
fi 

# copy over angular config
cat > $APP_ROOT/client/public/js/configs/config.js <<- EOM
angular.module('$APP_CLIENT')
.constant('GLOBAL',
    {hostname: '$APP_HOSTNAME',
     host: '$APP_HOST',
     port: '$HTTPS_PORT'}
);
EOM

# init the appropriate env database
export PORT=8080
grunt $1
if [ "$1" = "dev" ]; then
    echo "Running in dev mode"
    export DB=$APP_NAME"_test_db"
    export NODE_ENV="dev"
    npm start
elif [ "$1" = "int" ]; then
    echo "Running in int mode"
    export DB=$APP_NAME"_int_db"
    export NODE_ENV="int"
    npm start
elif [ "$1" = "stag" ]; then
    echo "Running in stag mode"
    export DB=$APP_NAME"_stag_db"
    export NODE_ENV="stag"
    npm start
elif [ "$1" = "prod" ]; then
    echo "Running in prod mode"
    export DB=$APP_NAME"_db"
    export NODE_ENV="prod"
    npm start
else
    echo "use dev, stag or int as argument"
fi