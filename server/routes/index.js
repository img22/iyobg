//-- Route to index function
exports.routeToIndex = function( req, res ) {
    res.render('index', { title: 'Welcome user!' });
};

//-- Render partials
exports.routeToPartials = function( req, res ) {
    var name = req.params.name;
    res.render('partials/' + name);
};
