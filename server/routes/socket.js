var config = require('../config/config').cfg;
var applogger = require('../applogger');
var log = applogger.logger.child({
    component: 'routes/socket'
});

// exports.handleUpdate = function(signal, data) {
//     log.info('handling socket update');
//     if(socketio) {
//         socketio.emit(signal, data);
//     } else {
//         log.info('socketio not ready');
//     }
// };

var authSockets = {};
var currentSocket = null;

var emitAll = function(word, data) {
    for(var k in authSockets) {
        authSockets[k].emit(word, data);
    }
};

exports.emitAll = emitAll;

exports.socketio = function() {
    return currentSocket;
};

exports.handleConnect = function(socketio) {

    currentSocket = socketio;

    var setUp = function(userId, socket) {

        // socket.on('example', function (from, msg) {
            
        // });

        socket.on('disconnect', function() {
            delete authSockets[socket.id];
        });
    };

    socketio.on('connection', function (socket) {
        log.info('connection from ' + socket.id);
        socket.on('auth', function(from, msg) {
            //...
        });

        setTimeout(function(){
            //If the socket didn't authenticate, disconnect it
            if (!socket.auth) {
              log.info("Disconnecting socket ", socket.id);
              delete authSockets[socket.id];
              socket.disconnect('unauthorized');
            }
        }, 1000);


    });
};