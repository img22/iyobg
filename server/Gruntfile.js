module.exports = function(grunt) {
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        
        // other configs
        uglify: {
            build: {
                files: {
                    '../client/public/dist/js/node_starter_client.min.js': [
                        '../client/public/js/**/*.js',
                        '../client/public/js/*.js'
                    ]
                }
            }
        },
        cssmin: {
            build: {
                files: {
                    '../client/public/dist/css/node_starter_client.min.css': [
                        '../client/public/css/*.css'
                    ]
                }
            }
        }
    });

    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-cssmin');

    grunt.registerTask('default', ['uglify', 'cssmin']);
    grunt.registerTask('prod', ['uglify', 'cssmin']);
    grunt.registerTask('dev', ['uglify', 'cssmin']);
    grunt.registerTask('stag', ['uglify', 'cssmin']);
    grunt.registerTask('int', ['uglify', 'cssmin']);
};